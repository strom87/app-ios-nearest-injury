//
//  LoginModel.swift
//  NearestInjury
//
//  Created by Daniel Ström on 31/10/15.
//  Copyright © 2015 Daniel Ström. All rights reserved.
//

import ObjectMapper

class LoginModel: Mappable {
    var email:String?
    var password:String?
    var platform:String? = "app"
    
    init() {
    }
    
    required init?(_ map: Map) {
    }
    
    func mapping(map: Map) {
        email    <- map["email"]
        password <- map["password"]
        platform <- map["platform"]
    }
    
    func toJson() -> String {
        return Mapper().toJSONString(self, prettyPrint: false)!
    }
}