//
//  HomeViewController.swift
//  NearestInjury
//
//  Created by Daniel Ström on 01/11/15.
//  Copyright © 2015 Daniel Ström. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(animated: Bool) {
        ApiProvider.instance.markers(Float64(11.978217), lat: Float64(57.707059), callback: {data, error in
            print(data)
        })
    }

}
