//
//  RegisterModel.swift
//  NearestInjury
//
//  Created by Daniel Ström on 01/11/15.
//  Copyright © 2015 Daniel Ström. All rights reserved.
//

import ObjectMapper

class RegisterModel: BaseModel {
    var name:String?
    var email:String?
    var password:String?
    var confPassword:String?
    
    override init() {
        super.init()
    }
    
    required init?(_ map: Map) {
        super.init(map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map)
        name         <- map["name"]
        email        <- map["email"]
        password     <- map["password"]
        confPassword <- map["conf_password"]
    }
    
    func toJson() -> String {
        return Mapper().toJSONString(self, prettyPrint: false)!
    }
}