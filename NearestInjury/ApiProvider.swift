//
//  ApiProvider.swift
//  NearestInjury
//
//  Created by Daniel Ström on 31/10/15.
//  Copyright © 2015 Daniel Ström. All rights reserved.
//

import Alamofire
import ObjectMapper

typealias RequestResponse = (data:NSDictionary, error:NSError?) -> Void

class ApiProvider {
    static let instance = ApiProvider()
    let domain:String = "http://ni-api.cloudapp.net:8080/"
//    let domain:String = "http://localhost:8080/"
    
    func login(model:LoginModel, callback: RequestResponse) {
        let url = self.url("login")
        
        Alamofire.request(.POST, url, parameters: self.jsonToDict(model.toJson()), encoding: .JSON).responseData{response in
            self.generateCallback(response.result.value, error: response.result.error, callback: callback)
        }
    }
    
    func register(model:RegisterModel, callback: RequestResponse) {
        let url = self.url("account/register")
        
        Alamofire.request(.POST, url, parameters: self.jsonToDict(model.toJson()), encoding: .JSON).responseData{response in
            self.generateCallback(response.result.value, error: response.result.error, callback: callback)
        }
    }
    
    func refreshToken(token:String, callback: RequestResponse) {
        let url = self.url("refresh-token")
        let data = ["refresh_token": token]
        
        Alamofire.request(.POST, url, parameters: data, encoding: .JSON).responseData{response in
            self.generateCallback(response.result.value, error: response.result.error, callback: callback)
        }
    }
    
    func markers(long:Float64, lat:Float64, callback: RequestResponse) {
        let url = self.url("marker/\(lat)/\(long)/100000")
        
        Alamofire.request(.GET, url, headers: self.authHeader()).responseData{response in
            self.generateCallback(response.result.value, error: response.result.error, callback: callback)
        }
    }
    
    func generateCallback(data:NSData?, error:NSError?, callback: RequestResponse) {
        if error != nil {
            callback(data: Dictionary<String, String>(), error: error)
            return
        }
        
        callback(data: self.dataToDict(data), error: nil)
    }
    
    func authHeader() -> [String:String]{
        let token = Auth.instance.getAuthToken()
        
        return ["Authorization": "Bearer \(token!)"]
    }
    
    func jsonToDict(jsonString: String) -> [String:AnyObject]? {
        if let data = jsonString.dataUsingEncoding(NSUTF8StringEncoding) {
            do {
                let json = try NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers) as? [String:AnyObject]
                return json
            } catch {
                print(error)
            }
        }
        
        return nil
    }
    
    func dataToDict(data:NSData?) -> NSDictionary {
        do {
            return try NSJSONSerialization.JSONObjectWithData(data!, options: .MutableLeaves) as! NSDictionary
        } catch {
            print(error)
        }
        
        return Dictionary<String, String>()
    }
    
    func url(route:String) -> String {
        return "\(self.domain)\(route)"
    }
}