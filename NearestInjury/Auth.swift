//
//  Auth.swift
//  NearestInjury
//
//  Created by Daniel Ström on 31/10/15.
//  Copyright © 2015 Daniel Ström. All rights reserved.
//

import JWTDecode
import ObjectMapper
import SwiftKeychainWrapper

class Auth {
    static let instance = Auth()
    let authTokenKey:String = "auth_token"
    let refreshTokenKey:String = "refresh_token"
    
    func isAuthenticated() -> Bool {
        if !self.hasAuthToken() {
            return false
        }
        
        return !self.isAuthTokenExpired()
    }
    
    func login(model:LoginModel, callback: (success:Bool) -> Void) {
        ApiProvider.instance.login(model, callback: {data, error in
            let response = Mapper<TokenModel>().map(data)
            
            if error != nil || response == nil || response!.success == false {
                callback(success: false)
                return
            }
            
            self.setTokens(response!.authToken!, refresh: response!.refreshToken!)
            callback(success: true)
        })
    }
    
    func refreshTokens(callback: (success:Bool) -> Void) {
        if !self.hasRefreshToken() {
            return callback(success: false)
        }
        
        ApiProvider.instance.refreshToken(self.getRefreshToken()!, callback: {data, error in
            let response = Mapper<TokenModel>().map(data)

            if error != nil || response == nil || response!.success == false {
                callback(success: false)
                return
            }
            
            self.setTokens(response!.authToken!, refresh: response!.refreshToken!)
            callback(success: true)
        })
    }
    
    func setTokens(auth:String, refresh:String) {
        KeychainWrapper.setString(auth, forKey: self.authTokenKey)
        KeychainWrapper.setString(refresh, forKey: self.refreshTokenKey)
    }
    
    func signOut() {
        KeychainWrapper.removeObjectForKey(self.authTokenKey)
        KeychainWrapper.removeObjectForKey(self.refreshTokenKey)
    }
    
    func getAuthToken() -> String? {
        return KeychainWrapper.stringForKey(self.authTokenKey)
    }
    
    func getRefreshToken() -> String? {
        return KeychainWrapper.stringForKey(self.refreshTokenKey)
    }
    
    func hasAuthToken() -> Bool {
        return self.getAuthToken() != nil
    }
    
    func hasRefreshToken() -> Bool {
        return self.getRefreshToken() != nil
    }
    
    func isAuthTokenExpired() -> Bool {
        let token = self.getAuthToken()!
        
        do {
            let jwt = try decode(token)
            let time = jwt.body["exp"] as! String
            
            return time.toUtcDate().isLessThan(NSDate())
        } catch {
            print(error)
        }
        
        return true
    }
}
