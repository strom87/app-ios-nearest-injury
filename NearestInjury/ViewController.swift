//
//  ViewController.swift
//  NearestInjury
//
//  Created by Daniel Ström on 01/11/15.
//  Copyright © 2015 Daniel Ström. All rights reserved.
//

import UIKit
import ObjectMapper

class ViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.emailTextField.text = "janne@nearestinjury.com"
        self.passwordTextField.text = "qwerty"
    }
    
    override func viewDidAppear(animated: Bool) {
        // If user have a refresh token and it is valid, login user
        if Auth.instance.hasRefreshToken() {
            Auth.instance.refreshTokens({success in
                if success {
                    self.performSegueWithIdentifier("homeView", sender: self)
                }
            })
        }
    }

    @IBAction func loginButton(sender: UIButton) {
        let model = LoginModel()
        model.email = self.emailTextField.text
        model.password = self.passwordTextField.text
        
        Auth.instance.login(model, callback: {success in
            if success {
                self.performSegueWithIdentifier("homeView", sender: self)
            }
        })
    }

    @IBAction func registerButton(sender: UIButton) {
        self.performSegueWithIdentifier("registerView", sender: self)
    }
}

