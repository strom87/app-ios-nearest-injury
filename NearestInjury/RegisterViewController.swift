//
//  RegisterViewController.swift
//  NearestInjury
//
//  Created by Daniel Ström on 01/11/15.
//  Copyright © 2015 Daniel Ström. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confPasswordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func registerButton(sender: UIButton) {
        let model = RegisterModel()
        model.name = self.usernameTextField.text
        model.email = self.emailTextField.text
        model.password = self.passwordTextField.text
        model.confPassword = self.confPasswordTextField.text
        
        ApiProvider.instance.register(model, callback: {data, error in
            print(data)
        })
    }
    
    @IBAction func backButton(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
