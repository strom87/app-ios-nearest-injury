//
//  TokenModel.swift
//  NearestInjury
//
//  Created by Daniel Ström on 31/10/15.
//  Copyright © 2015 Daniel Ström. All rights reserved.
//

import ObjectMapper

class TokenModel: BaseModel {
    var authToken:String?
    var refreshToken:String?
    
    override init() {
        super.init()
    }
    
    required init?(_ map: Map) {
        super.init(map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map)
        authToken    <- map["data.token.auth_token"]
        refreshToken <- map["data.token.refresh_token"]
    }
    
    func toJson() -> String {
        return Mapper().toJSONString(self, prettyPrint: false)!
    }
}
