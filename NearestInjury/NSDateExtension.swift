//
//  NSDateExtension.swift
//  NearestInjury
//
//  Created by Daniel Ström on 31/10/15.
//  Copyright © 2015 Daniel Ström. All rights reserved.
//

import Foundation

extension NSDate {
    
    func isGreaterThan(dateToCompare:NSDate) -> Bool {
        return self.compare(dateToCompare) == NSComparisonResult.OrderedDescending
    }
    
    func isLessThan(dateToCompare:NSDate) -> Bool {
        return self.compare(dateToCompare) == NSComparisonResult.OrderedAscending
    }
    
    func isEqualTo(dateToCompare:NSDate) -> Bool {
        return self.compare(dateToCompare) == NSComparisonResult.OrderedSame
    }
    
    func addDays(daysToAdd:Int) -> NSDate {
        let secondsInDays : NSTimeInterval = Double(daysToAdd) * 60 * 60 * 24
        return self.dateByAddingTimeInterval(secondsInDays)
    }
    
    func addHours(hoursToAdd:Int) -> NSDate {
        let secondsInHours = Double(hoursToAdd) * 60 * 60
        return self.dateByAddingTimeInterval(secondsInHours)
    }
    
}