//
//  StringExtension.swift
//  NearestInjury
//
//  Created by Daniel Ström on 31/10/15.
//  Copyright © 2015 Daniel Ström. All rights reserved.
//

import Foundation

extension String {
    
    func toUtcDate() -> NSDate {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        return formatter.dateFromString(self)!
    }
    
}
