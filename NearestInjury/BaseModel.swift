//
//  BaseModel.swift
//  NearestInjury
//
//  Created by Daniel Ström on 31/10/15.
//  Copyright © 2015 Daniel Ström. All rights reserved.
//

import ObjectMapper

class BaseModel: Mappable {
    var success:Bool?
    var message:String?
    
    init() {
    }
    
    required init?(_ map: Map) {
    }
    
    func mapping(map: Map) {
        success <- map["success"]
        message <- map["message"]
    }
}