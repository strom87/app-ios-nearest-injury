# Nearest injury app
An app running on iphone and ipad for ios 9

### Package manager
Uses **cocoapods** for managing packages.  
Install instructions: https://cocoapods.org/

### How to start project
Do not use the default **NearestInjury.xcodeproj** file to start the project.  
Instead use the **NearestInjury.xcworkspace** file created by cocoapod.